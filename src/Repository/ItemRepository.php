<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Item;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item|null findOneByData(string $data, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function findByUserAsArray(User $user): array
    {
        return $this->createQueryBuilder($alias = 'user')
            ->andWhere("{$alias}.user = :user")
            ->select([
                "{$alias}.id",
                "{$alias}.data",
                "{$alias}.createdAt AS created_at",
                "{$alias}.updatedAt AS updated_at"
            ])
            ->setParameter('user', $user)
            ->getQuery()
            ->getArrayResult();
    }
}
