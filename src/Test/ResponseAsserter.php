<?php

declare(strict_types=1);

namespace App\Test;

use Exception;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ResponseAsserter extends Assert
{
    private ?PropertyAccessor $accessor = null;

    /**
     * Asserts the array of property names are in the JSON response.
     *
     * @param array<int, string> $expectedProperties
     *
     * @throws Exception
     */
    public function assertResponsePropertiesExist(Response $response, array $expectedProperties): void
    {
        foreach ($expectedProperties as $propertyPath) {
            // this will blow up if the property doesn't exist
            $this->readResponseProperty($response, $propertyPath);
        }
    }

    /**
     * Asserts the specific propertyPath is in the JSON response.
     *
     * @param string $propertyPath e.g. firstName, battles[0].programmer.username
     *
     * @throws Exception
     */
    public function assertResponsePropertyExists(Response $response, string $propertyPath): void
    {
        // this will blow up if the property doesn't exist
        $this->readResponseProperty($response, $propertyPath);
    }

    /**
     * Asserts the given property path does *not* exist.
     *
     * @param string $propertyPath e.g. firstName, battles[0].programmer.username
     *
     * @throws Exception
     */
    public function assertResponsePropertyDoesNotExist(Response $response, string $propertyPath): void
    {
        try {
            // this will blow up if the property doesn't exist
            $this->readResponseProperty($response, $propertyPath);

            $this->fail(sprintf('Property "%s" exists, but it should not', $propertyPath));
        } catch (RuntimeException $e) {
            // cool, it blew up
            // this catches all errors (but only errors) from the PropertyAccess component
        }
    }

    /**
     * Asserts the response JSON property equals the given value.
     *
     * @param string $propertyPath  e.g. firstName, battles[0].programmer.username
     * @param mixed  $expectedValue
     *
     * @throws Exception
     */
    public function assertResponsePropertyEquals(
        Response $response,
        string $propertyPath,
        $expectedValue
    ): void {
        $actual = $this->readResponseProperty($response, $propertyPath);
        $this->assertEquals(
            $expectedValue,
            $actual,
            sprintf(
                'Property "%s": Expected "%s" but response was "%s"',
                $propertyPath,
                $expectedValue,
                var_export($actual, true)
            )
        );
    }

    /**
     * Asserts the specific response property contains the given value.
     *
     * e.g. "Hello world!" contains "world"
     *
     * @param string $propertyPath  e.g. firstName, battles[0].programmer.username
     * @param mixed  $expectedValue
     *
     * @throws Exception
     */
    public function assertResponsePropertyContains(
        Response $response,
        string $propertyPath,
        $expectedValue
    ): void {
        $actualPropertyValue = $this->readResponseProperty($response, $propertyPath);

        $this->assertContains(
            $expectedValue,
            $actualPropertyValue,
            sprintf(
                'Property "%s": Expected to contain "%s" but response was "%s"',
                $propertyPath,
                $expectedValue,
                var_export($actualPropertyValue, true)
            )
        );
    }

    /**
     * Reads a JSON response property and returns the value.
     *
     * This will explode if the value does not exist
     *
     * @return mixed
     *
     * @throws Exception
     * @throws AccessException
     */
    public function readResponseProperty(Response $response, string $propertyPath)
    {
        if (null === $this->accessor) {
            $this->accessor = PropertyAccess::createPropertyAccessor();
        }

        try {
            $data = json_decode($response->getContent(), true, 512, \JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $data = null;
        }

        if (null === $data) {
            throw new Exception(sprintf('Cannot read property "%s" - the response is invalid (is it HTML?)', $propertyPath));
        }

        try {
            return $this->accessor->getValue($data, $propertyPath);
        } catch (AccessException $e) {
            // it could be a stdClass or an array of stdClass
            $values = \is_array($data) ? $data : get_object_vars($data);

            throw new AccessException(sprintf('Error reading property "%s" from available keys (%s)', $propertyPath, implode(', ', array_keys($values))), 0, $e);
        }
    }
}
