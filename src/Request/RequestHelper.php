<?php

declare(strict_types=1);

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;

// Code based on ParseInputStream class
// https://gist.github.com/devmycloud/df28012101fbc55d8de1737762b70348
class RequestHelper
{
    public static function parseRequestContent(Request $request): void
    {
        $rawData = $request->getContent();

        // Fetch content and determine boundary
        $boundary = substr($rawData, 0, strpos($rawData, "\r\n"));
        // Fetch and process each part
        $parts = \array_slice(explode($boundary, $rawData), 1);
        foreach ($parts as $part) {
            // If this is the last part, break
            if ("--\r\n" === $part) {
                break;
            }
            // Separate content from headers
            $part                   = ltrim($part, "\r\n");
            [$rawHeaders, $content] = explode("\r\n\r\n", $part, 2);
            $content                = substr($content, 0, -2);
            // Parse the headers list
            $rawHeaders = explode("\r\n", $rawHeaders);
            $headers    = [];
            foreach ($rawHeaders as $header) {
                [$name, $value]             = explode(':', $header);
                $headers[strtolower($name)] = ltrim($value, ' ');
            }
            // Parse the Content-Disposition to get the field name, etc.
            if (isset($headers['content-disposition'])) {
                preg_match(
                    '/^form-data; *name="([^"]+)"(; *filename="([^"]+)")?/',
                    $headers['content-disposition'],
                    $matches
                );

                $fieldName = $matches[1];
                if (null === $fieldName) {
                    continue;
                }

                $request->attributes->set($fieldName, $content);
            }
        }
    }
}
