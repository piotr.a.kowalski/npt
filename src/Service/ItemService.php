<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Item;
use App\Repository\ItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ItemService
{
    private EntityManagerInterface $entityManager;
    private ItemRepository $itemRepository;

    public function __construct(EntityManagerInterface $entityManager, ItemRepository $itemRepository)
    {
        $this->entityManager  = $entityManager;
        $this->itemRepository = $itemRepository;
    }

    public function create(UserInterface $user, string $data): void
    {
        $item = new Item();
        $item->setUser($user);
        $item->setData($data);

        $this->entityManager->persist($item);
        $this->entityManager->flush();
    }

    public function update(UserInterface $user, int $id, string $data): void
    {
        $item = $this->itemRepository->findOneBy(['user' => $user, 'id' => $id]);

        if (null === $item) {
            return;
        }

        $item->setData($data);

        $this->entityManager->persist($item);
        $this->entityManager->flush();
    }
}
