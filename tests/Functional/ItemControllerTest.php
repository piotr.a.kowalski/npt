<?php

namespace App\Tests\Functional;

use App\Repository\ItemRepository;
use App\Test\ResponseAsserter;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class ItemControllerTest extends WebTestCase
{
    public function testCreate(): void
    {
        $client       = static::createClient();
        $jsonAsserter = new ResponseAsserter();

        $userRepository = static::$container->get(UserRepository::class);
        $itemRepository = static::$container->get(ItemRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);

        $data = 'very secure new item data';

        $newItemData = ['data' => $data];

        $client->request('POST', '/item', $newItemData);
        $client->request('GET', '/item');
        $response = $client->getResponse();

        self::assertResponseIsSuccessful();

        $jsonAsserter->assertResponsePropertyContains($response, '[0][data]', 'very secure new item data');

        self::assertNotNull($itemRepository->findOneByData($data));
    }

    public function testList(): void
    {
        $client       = static::createClient();
        $jsonAsserter = new ResponseAsserter();

        $userRepository = static::$container->get(UserRepository::class);
        $itemRepository = static::$container->get(ItemRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);

        $client->request('GET', '/item');
        self::assertResponseIsSuccessful();

        $response = $client->getResponse();

        $jsonAsserter->assertResponsePropertyContains($response, '[0][data]', 'very secure new item data');
    }

    public function testUpdate(): void
    {
        $client       = static::createClient();
        $jsonAsserter = new ResponseAsserter();

        $userRepository = static::$container->get(UserRepository::class);
        $itemRepository = static::$container->get(ItemRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);

        $oldData = 'very secure new item data';
        $item    = $itemRepository->findOneByData($oldData);
        self::assertNotNull($item);

        $id = $item->getId();

        $changedData = 'changed item data';

        $rawContent = <<<DATA
----------------------------088903867205600873682250\r\n
Content-Disposition: form-data; name="id"\r\n\r\n{$id}\n
----------------------------088903867205600873682250\r\n
Content-Disposition: form-data; name="data"\r\n\r\n{$changedData}\n
----------------------------088903867205600873682250--\r\n
DATA;

        $client->request('PUT', '/item', [], [], ['CONTENT_TYPE' => 'multipart/form-data'], $rawContent);
        $client->request('GET', '/item');
        $response = $client->getResponse();

        self::assertResponseIsSuccessful();

        $jsonAsserter->assertResponsePropertyContains($response, '[0][data]', $changedData);
    }

    public function testDelete(): void
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);
        $itemRepository = static::$container->get(ItemRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);

        $changedData = 'changed item data';
        $item        = $itemRepository->findOneByData($changedData);
        self::assertNotNull($item);
        $id = $item->getId();

        $client->request('DELETE', "/item/{$id}");
        self::assertResponseIsSuccessful();

        $client->request('GET', '/item');
        $response = $client->getResponse();

        try {
            $data = json_decode($response->getContent(), true, 512, \JSON_THROW_ON_ERROR);
        } catch (\Exception $e) {
            $data = null;
        }

        self::assertEmpty($data);
    }
}
