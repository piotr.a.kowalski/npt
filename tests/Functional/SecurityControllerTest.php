<?php

namespace App\Tests\Functional;

use App\Test\ResponseAsserter;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{
    public function testLogin(): void
    {
        $client       = static::createClient();
        $jsonAsserter = new ResponseAsserter();

        $loginData     = ['username' => 'john', 'password' => 'maxsecure'];
        $jsonLoginData = json_encode($loginData, JSON_THROW_ON_ERROR);

        $client->request(
            'POST',
            '/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $jsonLoginData
        );
        self::assertResponseIsSuccessful();
        $response = $client->getResponse();

        $jsonAsserter->assertResponsePropertyContains($response, '[username]', 'john');
        $jsonAsserter->assertResponsePropertyContains($response, '[roles][0]', 'ROLE_USER');
    }

    public function testLogout(): void
    {
        $client       = static::createClient();
        $jsonAsserter = new ResponseAsserter();

        $loginData     = ['username' => 'john', 'password' => 'maxsecure'];
        $jsonLoginData = json_encode($loginData, JSON_THROW_ON_ERROR);

        $client->request(
            'POST',
            '/logout',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $jsonLoginData
        );

        self::assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }
}
