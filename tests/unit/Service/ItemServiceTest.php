<?php

namespace App\Tests\Unit;

use App\Entity\Item;
use App\Entity\User;
use App\Repository\ItemRepository;
use App\Service\ItemService;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;

class ItemServiceTest extends TestCase
{
    /**
     * @var EntityManagerInterface|MockObject
     */
    private $entityManager;

    private ?ItemService $itemService = null;
    private ?ItemRepository $itemRepository = null;

    public function setUp(): void
    {
        /** @var EntityManagerInterface */
        $this->entityManager  = $this->createMock(EntityManagerInterface::class);
        $this->itemRepository = $this->createMock(ItemRepository::class);

        $this->itemService = new ItemService($this->entityManager, $this->itemRepository);
    }

    public function testCreate(): void
    {
        /** @var User */
        $user = $this->createMock(User::class);
        $data = 'secret data';

        $expectedObject = new Item();
        $expectedObject->setUser($user);
        $expectedObject->setData($data);

        $this->entityManager->expects($this->once())->method('persist')->with($expectedObject);

        $this->itemService->create($user, $data);
    }

    public function testUpdate(): void
    {
        /** @var User */
        $user = $this->createMock(User::class);
        $data = 'updated data';

        $expectedObject = $this->createMock(Item::class);
        $expectedObject->method('getId')->willReturn(1);
        $expectedObject->setUser($user);
        $expectedObject->setData($data);

        $this->itemRepository->method('findOneBy')->willReturn($expectedObject);

        $this->entityManager->expects($this->once())->method('persist')->with($expectedObject);

        $this->itemService->update($user, $expectedObject->getId(), $data);
    }
}
