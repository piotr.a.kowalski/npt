# Secure Information Storage REST API

### Project setup

* Add `secure-storage.localhost` to your `/etc/hosts`: `127.0.0.1 secure-storage.localhost`

* Run `make init` to initialize project

* Open in browser: http://secure-storage.localhost:8000/item Should get `Full authentication is required to access this resource.` error, because first you need to make `login` call (see `postman_collection.json` or `SecurityController` for more info).

### Run tests

make tests

### API credentials

* User: john
* Password: maxsecure

### Postman requests collection

You can import all available API calls to Postman using `postman_collection.json` file

### Endpoints
**Items**

1. `GET /item` get item list from logged in user
2. `POST /item` create new item for logged in user. Parameters: `id, data` (Pass data as form data paremeters `content-type = multipart/form-data`).
3. `PUT /item` update existing item for logged in user. Parameters: `id, data` (Pass data as form data paremeters `content-type = multipart/form-data`)
4. `DELETE /item/{id}` delete item witg given {id} parameter for logged in user. 

**Security**

1. `POST /login` login user. Parameters: `username, password` (Pass data in JSON format `content-type = application/json`)
1. `POST /logout` logout user. Parameters: `username, password` (Pass data in JSON format `content-type = application/json`)
