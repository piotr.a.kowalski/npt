<?php

declare(strict_types=1);

$finder = PhpCsFixer\Finder::create()->in(__DIR__ . '/src');

return (new PhpCsFixer\Config())
    ->setUsingCache(false)
    ->setFinder($finder)
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR2'                      => true,
        '@PSR12'                     => true,
        '@PSR12:risky'               => true,
        '@PHP74Migration'            => true,
        '@PHP74Migration:risky'      => true,
        '@DoctrineAnnotation'        => true,
        '@Symfony'                   => true,
        '@Symfony:risky'             => true,
        'align_multiline_comment'    => true,
        'array_syntax'               => ['syntax' => 'short'],
        'concat_space'               => ['spacing' => 'one'],
        'combine_consecutive_issets' => true,
        'combine_consecutive_unsets' => true,
        'binary_operator_spaces'     => ['operators' => ['=' => 'align_single_space', '=>' => 'align']],
        'no_extra_blank_lines'       => [
            'tokens' => [
                'curly_brace_block',
                'extra',
                'parenthesis_brace_block',
                'return',
                'square_brace_block',
                'throw',
                'use',
                'use_trait',
                'switch',
                'case',
                'default',
            ],
        ],
    ]);
